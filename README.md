# VWData Test Infra

## Prerequisites

Both virtual machines need (at minimum):
* openSSH-server installed
* python installed

To further configure the machines, please include the following groups in your /etc/ansible/hosts (on macOS ~/.ansible/hosts) file:
* `[vwdata_tse]`: location of the TSE machine
* `[vwdata_station_cbs]`: location of the cbs machine

Afterwards, create the following group of hosts:
```
[vwdata:children]
vwdata_station_cbs
vwdata_tse
```

You can add your SSH public key to the server (as a trusted SSH connection) by using this command:
```cat ~/.ssh/id_rsa.pub | ssh <user_account>@<server_address> 'mkdir ~/.ssh/ && cat >> ~/.ssh/authorized_keys'```

Optionally, (*and actually deprecated*) you can specify default SSH logins for the specific groups, for example:
```
[vwdata:vars]
ansible_connection=ssh
ansible_user=johan
ansible_ssh_pass=myPassword
```

### Downloading the needed docker containers

To donwload the docker containers locally, please perform the following commands:
```
docker save -o cbs/datasharing_cbs.tar datasharing/cbs
docker save -o um/datasharing_um.tar datasharing/um
docker save -o tse/datasharing_tse.tar datasharing/tse
```

These tarfiles are used by the [upload_docker_images.yml] playbook to upload them to the machines.

## Playbooks

### setup_general.yml
This playbook creates the basic infrastructure, installing and downloading the needed dependencies.

Command: `ansible-playbook --ask-become-pass setup_general.yml`

### setup_specific.yml
This playbook launches the services, including the config.json files (from the specific "cbs" and "tse" folder)

Command: `ansible-playbook setup_specific.yml`

### upload_docker_images.yml
This playbook reads the saved docker images in the cbs and tse folders, and uploads them into the corresponding machines (copy file to import folder on machine). This is part of our test infrastructure.

Command: `ansible-playbook upload_docker_images.yml`

### request_job_cbs.yml
This playbook sends a request object (*.ptmjob) to the CBS station. **Note that the ID and runId in the JSON file should be different for every request**.

### fetch_result_cbs.yml
This playbook fetches the result of the previously submitted .ptmjob object. It also sends the encrypted output to the input of the TSE machine. The result.txt file (saved under cbs/result/result.txt) contains the output of the docker image, in this case a JSON file containing the filename, symmetrical encryption key, and verification key.